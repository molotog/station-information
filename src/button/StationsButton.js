import React from 'react'

function StationsButton(props) {
    if (props.stationNames) {
        return null;
    }
    return(
        <div style={{paddingBottom: '10px'}}>
            <button className='button' onClick={props.onClick}>Show all stations</button>
        </div>
    )
}

export default StationsButton;