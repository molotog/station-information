import React from 'react';
import Form from './Form';
import StationInfo from './station/StationInfo';
import RequestErrors from './error/RequestErrors';
import StationsButton from './button/StationsButton';
import logo from './logo.svg';
import './App.css';

const stations_api_url = 'http://localhost:8080/';

class App extends React.Component {

    constructor() {
        super();
        this.state = {
            stationNames: null,
            station: null,
            stationName: '',
            requestFailed: false,
            error: null
        };
        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
        this.handleShowStations = this.handleShowStations.bind(this);
        this.handleErrors = this.handleErrors.bind(this);
    }

    handleErrors(response) {
        if (!response.ok) {
            response.json()
                .then(t => {
                    this.setState({
                        error: t.userMessage,
                        stationNames: null,
                        station: null,
                        requestFailed: true
                    })
                });
            throw Error(response.status);
        }
        return response;
    }

    componentWillMount() {
        this.handleShowStations();
    }

    handleShowStations() {

        fetch(stations_api_url + 'stations')
            .then(this.handleErrors)
            .then(d => d.json())
            .then(t => {
                this.setState({
                    stationNames: t,
                    station: null,
                    requestFailed: false
                })
            }).catch((err) => {
            console.log(err);
        });
    }

    handleChange(e) {
        this.setState({ stationName: e.target.value });
    }

    handleSubmit(e) {
        e.preventDefault();

        fetch(stations_api_url + this.state.stationName)
            .then(this.handleErrors)
            .then(d => d.json())
            .then(t => {
                this.setState({
                    station: t,
                    requestFailed: false,
                    stationNames: null
                })
            }).catch((err) => {
            console.log(err);
            });
    }

    render() {
        return (
            <div className="App">
                <header className="App-header">
                    <img src={logo} alt="logo"/>
                    <h1 className="App-title">Ilma andmed</h1>
                </header>
                <RequestErrors error={this.state.error} requestFailed={this.state.requestFailed}/>
                <StationsButton stationNames={this.state.stationNames} onClick={this.handleShowStations}/>
                <Form onSubmit={this.handleSubmit} onChange={this.handleChange} value={this.state.stationName}/>
                <StationInfo station={this.state.station} stationNames={this.state.stationNames}/>
            </div>
        );
    }
}

export default App;
