import React from 'react'
import './error.css'

function RequestErrors(props) {
    if (!props.requestFailed) {
        return null;
    }
    return(
        <div className={'error'}>{props.error}</div>
    )
}

export default RequestErrors;