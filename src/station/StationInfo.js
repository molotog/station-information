import React from 'react';
import {Table, TableHeader, TableRow, TableHeaderColumn, TableBody} from "material-ui";
import StationsList from './StationsList'
import './station-info.css'
import StationInfoTableRow from './StationInfoTableRow'

function StationInfo(props) {
    if (!props.station) {
        return <StationsList stationNames={props.stationNames}/>
    }
    return (
        <div className='wrap'>
            <Table>
                <TableHeader adjustForCheckbox={false} displaySelectAll={false}>
                    <TableRow>
                        <TableHeaderColumn style={{textAlign: 'center'}} colSpan="2">
                            <div className={'station-name'}>{props.station.name} station {!props.station.wmocode ? '' : '(wmcode=' + props.station.wmocode + ')'}</div>
                            <div className={'long-lat'}>lat: {props.station.latitude} long: {props.station.longitude}</div>
                        </TableHeaderColumn>
                    </TableRow>
                </TableHeader>
                <TableBody displayRowCheckbox={false}>
                    <StationInfoTableRow valueName="Chill Temperature" value={props.station.chillTemperature}/>
                    <StationInfoTableRow valueName="Phenomenon" value={props.station.phenomenon}/>
                    <StationInfoTableRow valueName="Visibility" value={props.station.visibility}/>
                    <StationInfoTableRow valueName="Precipitations" value={props.station.precipitations}/>
                    <StationInfoTableRow valueName="Air pressure" value={props.station.airpressure}/>
                    <StationInfoTableRow valueName="Relative humidity" value={props.station.relativehumidity}/>
                    <StationInfoTableRow valueName="Air temperature" value={props.station.airTemperature}/>
                    <StationInfoTableRow valueName="Wind direction" value={props.station.winddirection}/>
                    <StationInfoTableRow valueName="Wind speed" value={props.station.windSpeed}/>
                    <StationInfoTableRow valueName="Wind speed max" value={props.station.windspeedmax}/>
                    <StationInfoTableRow valueName="Water level" value={props.station.waterlevel}/>
                    <StationInfoTableRow valueName="Water temperature" value={props.station.watertemperature}/>
                </TableBody>
            </Table>
        </div>
    )
}

export default StationInfo;