import React from 'react';
import {List, ListItem} from "material-ui";

function StationsList(props) {
    if (!props.stationNames) {
        return null;
    }
    const items = [];
    props.stationNames
        .map((stationName, idx) => items.push(<ListItem disabled={true} key={stationName}>{stationName}</ListItem>));
    return (
        <List>{items}</List>
    );
}

export default StationsList;