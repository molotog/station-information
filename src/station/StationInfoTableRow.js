import React from 'react';
import {TableRow, TableRowColumn} from "material-ui";

function StationInfoTableRow(props) {
    if (!props.value) {
        return null;
    }
    return (
        <TableRow>
            <TableRowColumn>{props.valueName}</TableRowColumn>
            <TableRowColumn>{props.value}</TableRowColumn>
        </TableRow>
    )
}

export default StationInfoTableRow;