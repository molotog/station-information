import React from 'react';

function Form(props) {
    return (
        <form onSubmit={props.onSubmit}>
            <input onChange={props.onChange} value={props.value} />
            <button className='button'>Go for station</button>
        </form>
    );
}
export default Form